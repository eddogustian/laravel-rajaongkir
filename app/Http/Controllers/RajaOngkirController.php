<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
// Di atas, pertama kita import Facades Http atau Guzzle, karena kita akan menggunakan ini untuk melakukan request ke dalam endpoint RajaOngkir nantinya.

class RajaOngkirController extends Controller
{
    /**
     * API_KEY
     *
     * @var string
     */
    protected $API_KEY = 'bc339bdcdf217cf34d422b21e9db4d9f'; 

    /**
     * getProvinces
     * 
     * @return void
     */
    public function getProvinces()
    {
        $response = Http::withHeaders([
            'key' => $this->API_KEY
        ])->get('https://api.rajaongkir.com/starter/province');

        $provinces = $response['rajaongkir']['results'];

        return response()->json([
            'success' => true,
            'message' => 'Get All Provices',
            'data'    => $provinces
        ]);
    }

    /**
     * getCities
     * 
     * @param mixed $id
     * @return void
     */
    public function getCities($id)
    {
        $response = Http::withHeaders([
            'key' => $this->API_KEY
        ])->get('https://api.rajaongkir.com/starter/city?&province='.$id.'');

        $cities = $response['rajaongkir']['results'];

        return response()->json([
            'success' => true,
            'message' => 'Get City By ID Provinces : ' .$id,
            'data' => $cities
        ]);
    }

    /**
     * checkOngkir
     * 
     * @param mixed $request
     * @return void
     */
    
     public function checkOngkir(Request $request)
     {
        $response = Http::withHeaders([
            'key' => $this->API_KEY
         ])->post('https://api.rajaongkir.com/starter/cost',[
             'origin'       => $request->origin, //berisi ID kota/kabupate asal
             'destination'  => $request->destination, //berisi ID kota/kabupaten tujuan
             'weight'       => $request->weight, //berisi berat produk/barang (dalam satuan Gram)
             'courier'      => $request->courier // berisi nama kuris, seperti : jne, pos, tiki
         ]);

         $ongkir = $response['rajaongkir']['results'];

         return response()->json([
            'success' => true,
            'message' => 'Result Cost Ongkir',
            'data'    => $ongkir    
        ]);
     }
}
